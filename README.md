- For all Developers question
https://gitlab.com/tranggiang/backendtest/-/blob/main/src/main/java/com/example/demo/MainRun.java

- For Backend Developers

Student model : 
https://gitlab.com/tranggiang/backendtest/-/blob/main/src/main/java/com/example/demo/model/Student.java

Course model :
https://gitlab.com/tranggiang/backendtest/-/blob/main/src/main/java/com/example/demo/model/Course.java

CourseRegistration is joint table between Student and Course
https://gitlab.com/tranggiang/backendtest/-/blob/main/src/main/java/com/example/demo/model/CourseRegistration.java

- Overview
this is Spring boot  with H2 DB.

- To get List of Students in No Courses
http://localhost:8080/student

- To get Courses with more than 3 students.
http://localhost:8080/course