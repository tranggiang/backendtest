CREATE TABLE course (
  id int NOT NULL,
  name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE  student  (
   id int NOT NULL,
   name  varchar(255) DEFAULT NULL,
  PRIMARY KEY ( id )
) ;

CREATE TABLE  course_registration  (
   id  int NOT NULL,
   grade  int  DEFAULT NULL,
   registered_at  datetime DEFAULT NULL,
   course_id  int DEFAULT NULL,
   student_id  int DEFAULT NULL,
  PRIMARY KEY ( id ),

  CONSTRAINT  FKi5hvb7p3dafd9qd3k3eyeyann  FOREIGN KEY ( student_id ) REFERENCES  student  ( id ),
  CONSTRAINT  FKkcpyqpea6srulkxuhvrwrvhow  FOREIGN KEY ( course_id ) REFERENCES  course  ( id )
) ;