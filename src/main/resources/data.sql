INSERT INTO course (id,name) VALUES
	 (1,'math');
	 
 INSERT INTO student (id,name) VALUES
	 (1,'Alice'),
	 (2,'Bob'),
	 (3,'Carl');
	 
	 INSERT INTO course_registration (id,grade,registered_at,course_id,student_id) VALUES
	 (1,10,NULL,1,1),
	 (2,6,NULL,1,2),
	 (3,5,NULL,1,1);