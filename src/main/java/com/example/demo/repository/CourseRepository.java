package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
	@Query("select c from Course c where size(c.courseRegistration) >= 3")
	List<Course> getCoursesWithMoreThan3Students();
}
