package com.example.demo;

import java.util.Arrays;

public class MainRun {

	public static void main(String[] args) {
		int[] array1 = { 1, 3, 1, 4 };
		int[] array2 = { 1, 3, 1, 4, 4, 3, 1 };
		int[] array3 = { 3, 2, 2, 4 };
		int[] array4 = { 1, 3, 4, 3, 1, 4 };
		System.out.println(Arrays.toString(fix34(array2)));
	}

	public static int[] fix34(int[] nums) {
		int last4 = 1;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 3) {
				while (nums[last4] != 4) {
					++last4;
				}
				nums[last4++] = nums[i + 1];
				nums[i + 1] = 4;

			}
		}
		return nums;
	}
}
