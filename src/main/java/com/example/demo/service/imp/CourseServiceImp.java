package com.example.demo.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Course;
import com.example.demo.repository.CourseRepository;
import com.example.demo.service.CourseService;

@Service
public class CourseServiceImp implements CourseService {
	@Autowired
	private CourseRepository courseRepository;

	@Override
	public List<Course> getCoursesWithMoreThan3Students() {
		List<Course> courses = courseRepository.getCoursesWithMoreThan3Students();
		return courses;
	}

}
